black==22.3.0
django-cors-headers==3.11.0
Django==4.0.3
flake8==4.0.1
requests==2.28.0

