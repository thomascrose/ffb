import json
import requests
import os

SPORTS_DATA_KEY = os.environ["SPORTS_DATA_KEY"]

def get_team_data():
    url = f'https://api.sportsdata.io/v3/nfl/scores/json/Teams?key={SPORTS_DATA_KEY}'
    response = requests.get(url)
    content = json.loads(response.content)
    try:
        teams = []

        for team in content:
            teams.append({
              "name": team["Name"],
              "abbrev": team["Key"],
              "city": team["City"],
              "conference": team["Conference"],
              "division": team["Division"],
              "head_coach": team["HeadCoach"],
              "offensive_coordinator": team["OffensiveCoordinator"],
              "defensive_coordinator": team["DefensiveCoordinator"],
            })

        return teams
    except KeyError:
      return None

def get_schedule_data(year):
    url = f'https://api.sportsdata.io/v3/nfl/scores/json/Schedules/{year}?key={SPORTS_DATA_KEY}'
    response = requests.get(url)
    content = json.loads(response.content)
    try:
        games = []
        byes = []

        for game in content:
            if game["GameKey"] == None:
                byes.append({
                  "week": game["Week"],
                  "season": game["Season"],
                  "team": game["HomeTeam"], # Will need to get team instance in view
                })
                continue
            if (game["StadiumDetails"]["Type"] != "Outdoor"):
              is_in_dome = True
            else:
              is_in_dome = False

            games.append({
              "game_key": game["GameKey"],
              "season": game["Season"],
              "week": game["Week"],
              "home_team": game["HomeTeam"], # Will need to get team instance in view
              "away_team": game["AwayTeam"], # Will need to get team instance in view
              "date": game["Date"],
              "is_in_dome": is_in_dome
            })

        return {
          "games": games,
          "byes": byes,
        }
    except KeyError:
        return None

def get_player_data():
    url = f'https://api.sportsdata.io/v3/nfl/scores/json/Players?key={SPORTS_DATA_KEY}'
    response = requests.get(url)
    content = json.loads(response.content)

    positions = ["QB", "RB", "WR", "TE", "DL", "LB", "DB"]
    offense = ["QB", "RB", "WR", "TE"]
    defense = ["DL", "LB", "DB"]

    try:
        filtered_players = []
        players = []

        for player in content:
            if player["FantasyPosition"] in positions and player["CurrentTeam"] and player["Number"] and player["Active"] == True:
                filtered_players.append(player)

        for player in filtered_players:

            current_player = {
              "playerID": player["PlayerID"],
              "name": player["FirstName"] + " " + player["LastName"],
              "current_team": player["CurrentTeam"], # Will need to get team instance in view
              "position": player["FantasyPosition"],
              "number": player["Number"],
            }

            if player["FantasyPosition"] in offense:
                current_player["position_category"] = "OFF"
            if player["FantasyPosition"] in defense:
                current_player["position_category"] = "DEF"
            if player["Experience"] is not None:
                current_player["experience"] = player["Experience"]

            players.append(current_player)

        return players
    except KeyError:
        return None

def get_record_data(players):
    records = []
    for player in players:
        for i in range(17):
            url = f'https://api.sportsdata.io/v3/nfl/stats/json/PlayerGameStatsByPlayerID/2021/{i+1}/{player.playerID}?key={SPORTS_DATA_KEY}'
            try:
                response = requests.get(url)
                content = json.loads(response.content)
                records.append({
                  "player": content["PlayerID"],
                  "game": content["GameKey"],
                  "season": content["Season"],
                  "pass_attempts": content["PassingAttempts"],
                  "completions": content["PassingCompletions"],
                  "pass_yards": content["PassingYards"],
                  "pass_TDs": content["PassingTouchdowns"],
                  "interceptions": content["PassingInterceptions"],
                  "fumbles": content["Fumbles"],
                  "rush_attempts": content["RushingAttempts"],
                  "rush_yards": content["RushingYards"],
                  "rush_TDs": content["RushingTouchdowns"],
                  "targets": content["ReceivingTargets"],
                  "receptions": content["Receptions"],
                  "receiving_yards": content["ReceivingYards"],
                  "receiving_TDs": content["ReceivingTouchdowns"],
                  "tackles": content["SoloTackles"],
                  "assist_tackles": content["AssistedTackles"],
                  "sacks": content["Sacks"],
                  "passes_defended": content["PassesDefended"],
                  "fumbles_forced": content["FumblesForced"],
                  "fumbles_recovered": content["FumblesRecovered"],
                  "interceptions": content["Interceptions"],
                  "defensive_TDs": content["FumbleReturnTouchdowns"] + content["InterceptionReturnTouchdowns"],
                  "safeties": content["Safeties"],
                })
            except:
                continue
    return records
