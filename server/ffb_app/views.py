from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from zoneinfo import ZoneInfo
from .models import Player, Team, Game, Bye, Record
from .encoders import ModelEncoder
from .acls import get_team_data, get_schedule_data, get_player_data, get_record_data

class TeamShortEncoder(ModelEncoder):
    model = Team
    properties = [
        "name",
        "abbrev",
        "wins",
        "loses",
        "draws"
    ]

class TeamEncoder(ModelEncoder):
    model = Team
    properties = [
        "name",
        "abbrev",
        "city",
        "conference",
        "division",
        "head_coach",
        "offensive_coordinator",
        "defensive_coordinator",
        "wins",
        "loses",
        "draws",
    ]

class GameEncoder(ModelEncoder):
    model = Game
    properties = [
        "game_key",
        "week",
        "home_team",
        "away_team",
        "date",
        "is_in_dome",
    ]
    encoders = {
        "home_team": TeamShortEncoder(),
        "away_team": TeamShortEncoder(),
    }

class ByeEncoder(ModelEncoder):
    model = Bye
    properties = [
        "week",
        "team",
    ]
    encoders = {
        "team": TeamShortEncoder(),
    }

class PlayerEncoder(ModelEncoder):
    model = Player
    properties = [
        "playerID",
        "name",
        "current_team",
        "position",
        "position_category",
        "number",
        "experience",
        "active",
    ]
    encoders = {
        "current_team": TeamShortEncoder(),
    }

class RecordEncoder(ModelEncoder):
    model = Record
    properties = [
        "player",
        "game",
        "season",
        "pass_attempts",
        "completions",
        "pass_yards",
        "pass_TDs",
        "interceptions",
        "fumbles",
        "rush_attempts",
        "rush_yards",
        "rush_TDs",
        "targets",
        "receptions",
        "receiving_yards",
        "receiving_TDs",
        "tackles",
        "assist_tackles",
        "sacks",
        "passes_defended",
        "fumbles_forced",
        "fumbles_recovered",
        "interceptions",
        "defensive_TDs",
        "safeties",
    ]
    encoders = {
        "player": PlayerEncoder(),
        "game": GameEncoder(),
    }

@require_http_methods(["GET"])
def api_get_teams(request):
    teams = Team.objects.all()
    if teams.count() > 0:
        return JsonResponse(
            {"teams": teams},
            encoder=TeamEncoder,
        )
    else:
        data = get_team_data()
        try:
            for team in data:
                Team.objects.create(**team)
        except:
            return JsonResponse(
                {"message": "Error loading teams"},
                status=500,
            )

        teams = Team.objects.all()
        return JsonResponse(
            {"teams": teams},
            encoder=TeamEncoder,
        )

@require_http_methods(["GET"])
def api_get_games(request, year):
    games = Game.objects.filter(season=year)
    if games.count() > 0:
        return JsonResponse(
            {"games": games},
            encoder=GameEncoder
        )
    else:
        data = get_schedule_data(year)
        for game in data["games"]:
            try:
                home_team = Team.objects.get(abbrev=game["home_team"])
                away_team = Team.objects.get(abbrev=game["away_team"])
                game["home_team"] = home_team
                game["away_team"] = away_team
            except Team.DoesNotExist:
                return JsonResponse(
                    {"message": "Error loading schedule"},
                    status=500,
                )
            Game.objects.create(**game)

        games = Game.objects.filter(season=year)
        return JsonResponse(
            {"games": games},
            encoder=GameEncoder
        )

@require_http_methods(["GET"])
def api_get_byes(request, year):
    byes = Bye.objects.filter(season=year)
    if byes.count() > 0:
        return JsonResponse(
            {"byes": byes},
            encoder=ByeEncoder
        )
    else:
        data = get_schedule_data(year)
        for bye in data["byes"]:
            try:
                team = Team.objects.get(abbrev=bye["team"])
                bye["team"] = team
            except Team.DoesNotExist:
                return JsonResponse(
                    {"message": "Error loading schedule"},
                    status=500
                )
            Bye.objects.create(**bye)

        byes = Bye.objects.filter(season=year)
        return JsonResponse(
            {"byes": byes},
            encoder=ByeEncoder
        )

@require_http_methods(["GET"])
def api_get_players(request):
    players = Player.objects.all()
    if players.count() > 0:
        return JsonResponse(
            {"players": players},
            encoder=PlayerEncoder
        )
    else:
        data = get_player_data()
        for player in data:
            try:
                team = Team.objects.get(abbrev=player["current_team"])
                player["current_team"] = team
            except Team.DoesNotExist:
                return JsonResponse(
                    {"message": "Error loading players"},
                    status=500
                )
            Player.objects.create(**player)

        players = Player.objects.all()
        return JsonResponse(
            {"players": players},
            encoder=PlayerEncoder
        )

@require_http_methods(["GET"])
def api_get_records(request, id):
    # player = Player.objects.get(playerID=id)
    # records = player.records.all()
    players = Player.objects.all()
    data = get_record_data(list(players))
    for record in data:
        try:
            player = Player.objects.get(playerID=record["player"])
            record["player"] = player

            game = Game.objects.get(game_key=record["game"])
            record["game"] = game

        except Player.DoesNotExist:
            return JsonResponse(
                {"message": "Error finding player"},
                status=500
            )
        except Game.DoesNotExist:
            return JsonResponse(
                {"message": "Error finding game"},
                status=500
            )
        Record.objects.create(**record)

    records = Record.objects.filter(playerID=id)
    return JsonResponse(
        {"records": records},
        encoder=RecordEncoder
    )
