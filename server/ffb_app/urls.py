from django.urls import path

from .views import api_get_teams, api_get_games, api_get_byes, api_get_players, api_get_records

urlpatterns = [
  path('teams/', api_get_teams, name="teams"),
  path('games/<int:year>/', api_get_games, name="games"),
  path('byes/<int:year>/', api_get_byes, name="byes"),
  path('players/', api_get_players, name="players"),
  path('players/<int:id>/', api_get_records, name="records"),
]