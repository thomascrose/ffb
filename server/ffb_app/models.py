from types import CoroutineType
from django.db import models

class Team(models.Model):
    name = models.CharField(max_length=20)
    abbrev = models.CharField(max_length=3)
    city = models.CharField(max_length=50)
    conference = models.CharField(max_length=30)
    division = models.CharField(max_length=10)
    head_coach = models.CharField(max_length=50, null=True)
    offensive_coordinator = models.CharField(max_length=50, null=True)
    defensive_coordinator = models.CharField(max_length=50, null=True)
    wins = models.PositiveSmallIntegerField(default=0)
    loses = models.PositiveSmallIntegerField(default=0)
    draws = models.PositiveSmallIntegerField(default=0)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return f'{self.city} {self.name}'


class Game(models.Model):
    game_key = models.CharField(max_length=20)
    season = models.PositiveSmallIntegerField()
    week = models.PositiveSmallIntegerField()
    home_team = models.ForeignKey(
      Team,
      related_name="home_games",
      on_delete=models.CASCADE
    )
    away_team = models.ForeignKey(
      Team,
      related_name="away_games",
      on_delete=models.CASCADE
    )
    date = models.DateTimeField()
    is_in_dome = models.BooleanField(default=False)

    class Meta:
        ordering = ['-season', 'week']

    def __str__(self):
        return f'Season: {self.season} - Week: {self.week} - {self.home_team} vs. {self.away_team}'

class Bye(models.Model):
    week = models.PositiveSmallIntegerField()
    season = models.PositiveSmallIntegerField()
    team = models.ForeignKey(
      Team,
      related_name="byes",
      on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ['-season', 'week']

    def __str__(self):
        return f'Season: {self.season} - Bye: {self.week} - {self.team}'

class Player(models.Model):
    playerID = models.PositiveSmallIntegerField()
    name = models.CharField(max_length=100)
    current_team = models.ForeignKey(
      Team,
      related_name="players",
      on_delete=models.PROTECT,
    )
    position = models.CharField(max_length=10)
    position_category = models.CharField(max_length=3)
    number = models.PositiveSmallIntegerField()
    experience = models.PositiveSmallIntegerField(default=0)
    active = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.name} - {self.current_team.name}'

class Record(models.Model):
    player = models.ForeignKey(
      Player,
      related_name="records",
      on_delete=models.CASCADE,
    )
    game = models.ForeignKey(
      Game,
      related_name="records",
      on_delete=models.CASCADE,
    )
    season = models.CharField(max_length=4)
    pass_attempts = models.PositiveSmallIntegerField()
    completions = models.PositiveSmallIntegerField()
    pass_yards = models.SmallIntegerField()
    pass_TDs = models.PositiveSmallIntegerField()
    interceptions = models.PositiveSmallIntegerField()
    fumbles = models.PositiveSmallIntegerField()
    rush_attempts = models.PositiveSmallIntegerField()
    rush_yards = models.SmallIntegerField()
    rush_TDs = models.PositiveSmallIntegerField()
    targets = models.PositiveSmallIntegerField()
    receptions = models.PositiveSmallIntegerField()
    receiving_yards = models.SmallIntegerField()
    receiving_TDs = models.PositiveSmallIntegerField()
    tackles = models.PositiveSmallIntegerField()
    assist_tackles = models.PositiveSmallIntegerField()
    sacks = models.PositiveSmallIntegerField()
    passes_defended = models.PositiveSmallIntegerField()
    fumbles_forced = models.PositiveSmallIntegerField()
    fumbles_recovered = models.PositiveSmallIntegerField()
    interceptions = models.PositiveSmallIntegerField()
    defensive_TDs = models.PositiveSmallIntegerField()
    safeties = models.PositiveSmallIntegerField()

    class Meta:
        ordering = ['-season', 'game', 'player']

    def __str__(self):
        return f'{self.player.name} - {self.game.season} - {self.game.week}'
