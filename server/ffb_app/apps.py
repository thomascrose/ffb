from django.apps import AppConfig


class FfbAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ffb_app'
