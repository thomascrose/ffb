import { Fragment, useState, useEffect } from 'react'

function Byes() {
  const [byes, getByes] = useState([])
  const [sortedByes, getSortedByes] = useState([])
  const [selected, setSelected] = useState({team: {}})
  useEffect(() => {

    const fetchData = async () => {
      const url = 'http://localhost:9000/api/byes/'
      const response = await fetch(url)

      if (response.ok) {
        const data = await response.json()
        getByes(data.byes)

        const dataCopy = [...data.byes]
        const sortedData = dataCopy.sort((a, b) => {
          if (a.team.abbrev < b.team.abbrev) {
            return -1
          }
          if (b.team.abbrev < a.team.abbrev) {
            return 1
          }
          return 0
        })
        getSortedByes(sortedData)
      }
    }
    fetchData()
  }, [])

  return (
    <Fragment>
      <h1>Bye Weeks</h1>
      <div className="btn-group">
        <button type="button" className="btn btn-info btn-sm dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
          Find A Team
        </button>
        <ul className="dropdown-menu">
          {sortedByes.map(bye => {
            return (
              <li key={bye.team.abbrev} className="dropdown-item" onClick={() => setSelected(bye)}>
                {bye.team.abbrev}
              </li>
            )
          })}
        </ul>
      </div>
      <table className="table table-striped table-hover">
        <thead>
          <tr>
            <th>WK</th>
            <th>Team</th>
          </tr>
        </thead>
        <tbody>
          {byes.map(bye =>
            bye.team.abbrev === selected.team.abbrev ?
            <tr key={bye.team.abbrev} className="table-primary">
              <td>{bye.week}</td>
              <td>{bye.team.abbrev}</td>
            </tr>
             :
            <tr key={bye.team.abbrev}>
              <td>{bye.week}</td>
              <td>{bye.team.abbrev}</td>
            </tr>
          )}
        </tbody>
      </table>
    </Fragment>
  )
}

export default Byes