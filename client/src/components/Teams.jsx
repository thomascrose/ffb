import { Fragment, useState, useEffect } from 'react'

function Teams() {
  const [teams, setTeams] = useState([])
  useEffect(() => {

    const fetchData = async () => {
      const url = `http://localhost:9000/api/teams/`
      const response = await fetch(url)

      if (response.ok) {
        const data = await response.json()
        setTeams(data.teams)
      }
    }
    fetchData()
  }, [])

  return (
    <Fragment>
      <h1>Teams</h1>
      <table className="table table-striped table-hover">
        <thead>
          <tr>
            <th>Team Name</th>
            <th>Abbreviation</th>
            <th>City</th>
            <th>Conference</th>
            <th>Division</th>
            <th>Coach</th>
            <th>OC</th>
            <th>DC</th>
            <th>Current Record</th>
          </tr>
        </thead>
        <tbody>
          {teams.map(team => {
            return (
              <tr>
                <td>{team.name}</td>
                <td>{team.abbrev}</td>
                <td>{team.city}</td>
                <td>{team.conference}</td>
                <td>{team.division}</td>
                <td>{team.head_coach}</td>
                <td>{team.offensive_coordinator}</td>
                <td>{team.defensive_coordinator}</td>
                <td>{team.wins}-{team.loses}-{team.draws}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </Fragment>
  )
}

export default Teams