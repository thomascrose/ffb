import { Fragment, useState, useEffect } from 'react'

function Players() {
  const [players, setPlayers] = useState([])
  const [teams, setTeams] = useState([])
  const [positions, setPositions] = useState([])
  const [filteredPlayers, filterPlayers] = useState([])
  const [team, selectTeam] = useState()
  const [position, selectPosition] = useState()

  useEffect(() => {
    const fetchData = async () => {
      const url = `http://localhost:9000/api/players/`
      const response = await fetch(url)

      if (response.ok) {
        const data = await response.json()
        const players=  data.players
        setPlayers(players)

        const uniqueTeams = new Set()
        const uniquePositions = new Set()

        players.forEach(player => {
          uniqueTeams.add(player.current_team.abbrev)
          uniquePositions.add(player.position)
        })

        const sortedTeams = Array.from(uniqueTeams).sort((a, b) => {
          if (a < b) {
            return -1
          } else if (a > b) {
            return 1
          } else {
            return 0
          }
        })
        const sortedPositions = Array.from(uniquePositions).sort((a, b) => {
          if (a < b) {
            return -1
          } else if (a > b) {
            return 1
          } else {
            return 0
          }
        })

        setTeams(sortedTeams)
        setPositions(sortedPositions)
      }
    }
    fetchData()
  }, [])

  useEffect(() => {
    filterPlayers(players.filter(player => {
      if (!position) {
        return player.current_team.abbrev === team
      }
      if (!team) {
        return player.position === position
      }
      return (
        player.current_team.abbrev === team && player.position === position
      )
    }
    ))
  }, [team, position, players])


  return (
    <Fragment>
      <h1>Players</h1>
      <div className="btn-group">
        <button type="button" className="btn btn-info btn-sm dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
          Filter By Team
        </button>
        <ul className="dropdown-menu">
          {teams.map(team => {
            return (
              <li key={team} className="dropdown-item"
              onClick={() => {
                  selectTeam(team)
                }}>
                {team}
              </li>
            )
          })}
        </ul>
      </div>
      <div className="btn-group">
        <button type="button" className="btn btn-info btn-sm dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
          Filter By Position
        </button>
        <ul className="dropdown-menu">
          {positions.map(position => {
            return (
              <li key={position} className="dropdown-item"
              onClick={() => {
                  selectPosition(position)
                }}>
                {position}
              </li>
            )
          })}
        </ul>
      </div>
      <table className="table table-striped table-hover">
        <thead>
          <tr>
            <th>Player</th>
            <th>Team</th>
            <th>Position</th>
            <th>Number</th>
            <th>Experience</th>
          </tr>
        </thead>
        <tbody>
          { team || position ?
          filteredPlayers.map(player => {
            return (
              <tr>
                <td>{player.name}</td>
                <td>{player.current_team.abbrev}</td>
                <td>{player.position}</td>
                <td>{player.number}</td>
                <td>{player.experience}</td>
              </tr>
            )
          })
          :
          players.map(player => {
            return (
              <tr>
                <td>{player.name}</td>
                <td>{player.current_team.abbrev}</td>
                <td>{player.position}</td>
                <td>{player.number}</td>
                <td>{player.experience}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </Fragment>
  )
}

export default Players