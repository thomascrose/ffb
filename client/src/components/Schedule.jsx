import { Fragment, useState, useEffect } from 'react'

function Schedule() {
  const [schedule, getSchedule] = useState([])
  const [filteredSchedule, filterSchedule] = useState([])
  const [byes, getByes] = useState([])
  const [week, selectWeek] = useState()
  const [team, selectTeam] = useState()

  useEffect(() => {

    const fetchData = async () => {
      const gamesUrl = 'http://localhost:9000/api/games/'
      const byesUrl = 'http://localhost:9000/api/byes/'
      const gamesResponse = await fetch(gamesUrl)
      const byesResponse = await fetch(byesUrl)

      if (gamesResponse.ok) {
        const gameData = await gamesResponse.json()
        getSchedule(gameData.games)
      }

      if (byesResponse.ok) {
        const byeData = await byesResponse.json()
        getByes(byeData.byes)
      }

    }
    fetchData()
  }, [])

  useEffect(() => {
    filterSchedule(schedule.filter(game => game.week === week))
  }, [week, schedule])

  useEffect(() => {
    filterSchedule(schedule.filter(game => game.home_team.abbrev === team || game.away_team.abbrev === team))
  }, [team, schedule])

  return (
    <Fragment>
      <h1>Schedule</h1>
      <div className="btn-group">
        <button type="button" className="btn btn-info btn-sm dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
          Filter By Week
        </button>
        <ul className="dropdown-menu">
          {[...Array(18).keys()].map(seasonWeek => {
            return (
              <li key={seasonWeek + 1} className="dropdown-item"
              onClick={() => {
                  selectWeek(seasonWeek + 1)
                }}>
                {seasonWeek + 1}
              </li>
            )
          })}
        </ul>
      </div>
      <div className="btn-group">
        <button type="button" className="btn btn-info btn-sm dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
          Filter By Team
        </button>
        <ul className="dropdown-menu">
          {byes.map(bye => {
            return (
              <li key={bye.team.abbrev} className="dropdown-item"
              onClick={() => {
                  selectTeam(bye.team.abbrev)
                }}>
                {bye.team.abbrev}
              </li>
            )
          })}
        </ul>
      </div>
      <table className="table table-striped table-hover">
        <thead>
          <tr>
            <th>WK</th>
            <th>Home Team</th>
            <th>Away Team</th>
            <th>Playing</th>
          </tr>
        </thead>
        <tbody>
          { week || team ? filteredSchedule.map(game => {
            const date = new Date(game.date);
            const day = date.toDateString();
            return (
              <tr key={game.game_key}>
                <td>{game.week}</td>
                <td>{game.home_team.abbrev}</td>
                <td>{game.away_team.abbrev}</td>
                <td>{day}</td>
              </tr>
            )
            })
            :
            schedule.map(game => {
              const date = new Date(game.date);
              const day = date.toDateString();
              return (
                <tr key={game.game_key}>
                  <td>{game.week}</td>
                  <td>{game.home_team.abbrev}</td>
                  <td>{game.away_team.abbrev}</td>
                  <td>{day}</td>
                </tr>
              )
            }
            )
          }
          {/* <tr>
            <td align="center" colSpan={4}>ON BYE</td>
          </tr>
          {
            byes.filter(game => game.week === week).length > 0 ?
              byes.filter(game => game.week === week).map(game => {
                return (
                  <tr key={game.team.abbrev}>
                    <td align="center" colSpan={4}>{game.team.abbrev}</td>
                  </tr>
                )
              })
              :
              <tr>
                <td align="center" colSpan={4}>No Teams On Bye This Week</td>
              </tr>
          } */}
        </tbody>
      </table>
    </Fragment>
  )

}

export default Schedule
