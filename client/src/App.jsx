import { Fragment } from 'react'
import Nav from './components/Nav.jsx'
import MainPage from './components/MainPage.jsx'
import Byes from './components/Byes.jsx'
import Schedule from './components/Schedule.jsx'
import Teams from './components/Teams.jsx'
import Players from './components/Players.jsx'
import { Routes, Route } from "react-router-dom";

function App() {
  return (
    <Fragment>
      <Nav />
      <Routes>
        <Route path="/" element={<MainPage />} />
        <Route path="/byes" element={<Byes />} />
        <Route path="/schedule" element={<Schedule />} />
        <Route path="/teams" element={<Teams />} />
        <Route path="/players" element={<Players />} />
      </Routes>
    </Fragment>
  );
}

export default App;
